const
    // development mode
    devBuild = true,

    //modules
    gulp = require('gulp'),
    noop = require('gulp-noop'),
    newer = require('gulp-newer'),
    imagemin = require('gulp-imagemin'),
    htmlclean = require('gulp-htmlclean'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    browserSync = require('browser-sync').create(),

    // folders
    src = 'src/',
    build = 'public/'
    
// images processing
function images(){

    const input = src + 'images/**/*';
    const output = build + 'assets/images/';

    return gulp.src(input)
        .pipe(newer(output))
        .pipe(imagemin())
        .pipe(gulp.dest(output));
}

// html processing
function html(){
    const input = src + 'html/**/*';
    const output = build ;

    return gulp.src(input)
        .pipe(newer(output))
        .pipe(devBuild ? noop() : htmlclean)
        .pipe(gulp.dest(output));
}


// CSS processing
function css(){
    const input = src + 'scss/main.scss';
    const output = build + 'assets/css/';

    return gulp.src(input)
        .pipe(newer(output))
        .pipe(devBuild ? sourcemaps.init() : noop())
        .pipe(sass({outputStyle:'compressed'}).on('error', sass.logError))
        .pipe(concat('bundle.css')).pipe(devBuild ? sourcemaps.write() : noop())
        .pipe(rename({suffix:'.min'}))
        .pipe(gulp.dest(output));
}
 // javascript processing
 function js(){
    const input = src + 'js/**/*';
    const output = build + 'assets/js/';

    return gulp.src(input)
        .pipe(newer(output))
        .pipe(noop())
        .pipe(gulp.dest(output));
 }

 //watch for file changes
function watch(done){

    browserSync.init({
        server: {
            baseDir: './' + build,
            startPath: 'index.html'
        }
    });
    //image changes
    gulp.watch(src + 'images/**/*', images);
    //html changes
    gulp.watch(src + 'html/**/*', html);
    //css changes
    gulp.watch(src + 'scss/**/*', css);
    //javascript changes
    gulp.watch(src + 'js/**/*', js);

    done();
}

// create single tasks
exports.html = gulp.series(images, html);
exports.css = css;
exports.js = js;
exports.watch = watch;

// create build task
exports.build = gulp.parallel(exports.html, exports.css, exports.js);

//defaut task
exports.default = gulp.series(exports.build, exports.watch);